package recommendation;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class RecSearchLocation extends StrategySearchLocation {

	@Override
	public String getSearchLocation(String recWeight, Set<String> location_applied, Set<String> location_liked) {
		// TODO Auto-generated method stub
		double weightForApply = Double.valueOf(recWeight);
		double weightForLike = 1 - weightForApply;
		Map<String, Double> selectionMap = new HashMap<String, Double>();
		for (String location : location_applied) {
			selectionMap.put(location + "&ap", weightForApply);
		}
		for (String location : location_liked) {
			selectionMap.put(location + "&lk", weightForLike);
		}
		Random random = new Random();
		String location = RandomWeightedUtil.getWeightedRandom(selectionMap, random);
		String realLocation = location.substring(0, location.length() - 3);
		return realLocation;
	}		

}
