package recommendation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class RecSearchTerm extends StrategySearchTerm {

	@Override
	public String getSearchTerm(String recWeight, Set<String> applied_term, Set<String> liked_term) {
		/// TODO Auto-generated method stub
		double weightForApply = Double.valueOf(recWeight);
		double weightForLike = 1 - weightForApply;
		Map<String, Double> selectionMap = new HashMap<String, Double>();
		for (String term : applied_term) {
			selectionMap.put(term + "&ap", weightForApply);
		}
		for (String location : liked_term) {
			selectionMap.put(location + "&lk", weightForLike);
		}
		Random random = new Random();
		String term = RandomWeightedUtil.getWeightedRandom(selectionMap, random);
		String reallocation = term.substring(0, term.length() - 3);
		return reallocation;
	}

}
