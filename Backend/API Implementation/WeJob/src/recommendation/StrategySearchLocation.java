package recommendation;

import java.util.List;
import java.util.Set;

public abstract class StrategySearchLocation implements RecommendationStrategy {
	public void solve() {
		//leave for future work
	}
	
	public abstract String getSearchLocation(String recWeight, Set<String> location_liked, Set<String> location_applied);
}
