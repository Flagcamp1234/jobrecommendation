package recommendation;

import java.util.List;
import java.util.Set;

public abstract class StrategySearchTerm implements RecommendationStrategy {
	
	public void solve() {
		//leave for future work
	}
	
	public abstract String getSearchTerm(String recWeight, Set<String> term_applied, Set<String> term_liked);
}
