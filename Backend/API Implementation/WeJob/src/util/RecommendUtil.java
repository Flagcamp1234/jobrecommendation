package util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import db.DBConnection;
import db.DBConnectionFactory;
import entity.Job;
import recommendation.RecSearchLocation;
import recommendation.RecSearchTerm;
import recommendation.StrategySearchLocation;
import recommendation.StrategySearchTerm;

public class RecommendUtil {
	public static List<Job> recommend(String username, String recWeight, String useLocation) {
		List<Job> list = new ArrayList<Job>();
		StrategySearchLocation ssl = new RecSearchLocation();
		StrategySearchTerm sst = new RecSearchTerm();
		DBConnection conn = DBConnectionFactory.getConnection();
		Set<Job> jobs_applied = conn.getAppliedItems(username);
		Set<Job> jobs_liked = conn.getFavoriedItems(username);
		
		Set<String> term_applied = getSearchTerm(jobs_applied);
		Set<String> term_liked = getSearchTerm(jobs_liked);
		
		Set<String> location_applied = getSearchLocation(jobs_applied);
		Set<String> location_liked = getSearchLocation(jobs_liked);
		
		String searchTerm = sst.getSearchTerm(recWeight, term_applied, term_liked);
		String location = "";
		if (Boolean.valueOf(useLocation)) {
			location = ssl.getSearchLocation(recWeight, location_applied, location_liked);
		}
		//call Indeed Job Search API to get Job Title, Job Snippet, Job Detail, Job ID, Location, Company name
		list = new SearchUtil().search(searchTerm, location, username);
		return list;
	}
	
	private static Set<String> getSearchTerm(Set<Job> jobs) {
		Set<String> term = new HashSet<String>();
		jobs.forEach(job -> {term.add(job.getJobTitle());});
		return term;
	}
	
	private static Set<String> getSearchLocation(Set<Job> jobs) {
		Set<String> term = new HashSet<String>();
		jobs.forEach(job -> {term.add(job.getLocation());});
		return term;
	}
}
