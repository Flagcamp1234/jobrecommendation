package util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import db.DBConnection;
import db.DBConnectionFactory;
import entity.Job;
import entity.Job.JobBuilder;

public class SearchUtil {
	private static final String SEARCH_URL = "http://api.indeed.com/ads/apisearch";
	private static final String JOB_URL = "http://api.indeed.com/ads/apigetjobs";
	private static final String PUBLISHER = "125768650773419";
	private static final String VERSION = "2";
	private static final String FORMAT = "json"; // data format
	private static final String DEFAULT_KEYWORD = ""; // no restriction
	private static final String DEFAULT_LOC = "";
	/**
	 * This method will return the job list via Indeed API and ensure it does not include the Job already applied or favored
	 * @param keyword
	 * @param loc
	 * @param username
	 * @return
	 */
	public List<Job> search(String keyword, String loc, String username) {
		if (keyword == null) {
			keyword = DEFAULT_KEYWORD;
		}
		if (loc == null) {
			loc = DEFAULT_LOC;		
		}		
		try {
			keyword = URLEncoder.encode(keyword, "UTF-8");
			loc = URLEncoder.encode(loc, "UTF-8");			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		List<String> usedJobID = new ArrayList<String>();
		usedJobID = getUsedJobID(username);
		final int COUNT = 10; int default_search_count = 100;
		if (usedJobID.size() + COUNT >  default_search_count) {
			default_search_count = usedJobID.size() + COUNT;
		}
		
		String query = String.format("publisher=%s&q=%s&l=%s&v=%s&format=%s&limit=%s", PUBLISHER, keyword, loc, VERSION, FORMAT, String.valueOf(default_search_count));
		String url = SEARCH_URL + "?" + query;
		
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestMethod("GET");
			int responseCode = connection.getResponseCode();
			System.out.println("Sending request to url: " + url);
			System.out.println("Response code: " + responseCode);
			
			if (responseCode != 200) {
				return new ArrayList<>();
			}
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			StringBuilder response = new StringBuilder();
			
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
			reader.close();
			JSONObject obj = new JSONObject(response.toString());
			if (!obj.isNull("results")) {
				return getJobList(obj.getJSONArray("results"), usedJobID);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}
	
	private List<String> getUsedJobID(String username) {
		Set<String> result = new HashSet<String>();
		DBConnection conn = DBConnectionFactory.getConnection();
		Set<Job> favoredjobs = conn.getFavoriedItems(username);
		Set<Job> likedjobs = conn.getAppliedItems(username);
		favoredjobs.forEach(p -> result.add(p.getApiJobId()));
		likedjobs.forEach(p -> result.add(p.getApiJobId()));
		return new ArrayList<String>(result);
	}
	/**
	public List<Job> getJob(String[] jobIds) {
		if(jobIds == null || jobIds.length == 0) return new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < jobIds.length; i++) {
			sb.append(jobIds[i]);
			if(i < jobIds.length - 1) {
				sb.append(",");
			}			
		}
		String jobKeys = sb.toString();
		
		// http://api.indeed.com/ads/apigetjobs?publisher=125768650773419&jobkeys=17c50aa2b125273f&v=2&format=json
		String query = String.format("publisher=%s&jobkeys=%s&v=%s&format=%s", PUBLISHER, jobKeys, VERSION, FORMAT);
		String url = JOB_URL + "?" + query;
		
		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestMethod("GET");
			
			int responseCode = connection.getResponseCode();
			System.out.println("Sending request to url: " + url);
			System.out.println("Response code: " + responseCode);
			
			if (responseCode != 200) {
				return new ArrayList<>();
			}
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			StringBuilder response = new StringBuilder();
			
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
			reader.close();
			JSONObject obj = new JSONObject(response.toString());
			if (!obj.isNull("results")) {
				return getJobList(obj.getJSONArray("results"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}
	*/
	private List<Job> getJobList(JSONArray jobs, List<String> usedJobID) throws JSONException{
		List<Job> jobList = new ArrayList<>();
		int count = 10; //ensure the search result count = 10 since pagination is not done.
		for(int i = 0; i < jobs.length(); i++) {
			JSONObject job = jobs.getJSONObject(i);
			JobBuilder builder = new JobBuilder();
			if(!usedJobID.contains(job.getString("jobkey")) && count > 0) {
				if(!job.isNull("jobkey")) {
					builder.setApiJobId(job.getString("jobkey"));
				}
				if(!job.isNull("jobtitle")) {
					builder.setJobTitle(job.getString("jobtitle"));
				}
				if (!job.isNull("company")) {
					builder.setCompany(job.getString("company"));
				}
				if (!job.isNull("snippet")) {
					builder.setJobSnippet(job.getString("snippet"));
				}
				if (!job.isNull("url")) {
					builder.setJobUrl(job.getString("url"));
				}
				builder.setLocation(getLocation(job));			
				jobList.add(builder.build());
				count--;
			}
		}
		
		return jobList;
	}
	
	private String getLocation(JSONObject job) throws JSONException{
		StringBuilder builder = new StringBuilder();
		if(!job.isNull("city")) {
			builder.append(job.getString("city"));
		}
		if(!job.isNull("state")) {
			builder.append(",");
			builder.append(job.getString("state"));
		}
		String result = builder.toString();
		if(!result.isEmpty()) {
			return result;
		}
		return "";
	}	
	
	/**
	//functions below are for testing purpose
	private void searchAPI(String keyword, String loc) {
		List<Job> jobs = search(keyword, loc);
		for(Job job : jobs) {
			System.out.println(job.toJSONObject());
			
		}
	}
	
	private void getJobAPI(String[] jobIds) {
		List<Job> jobs = getJob(jobIds);
		for(Job job : jobs) {
			System.out.println(job.toJSONObject());			
		}
	}
	
	public static void main(String[] args) {
		SearchUtil idApi = new SearchUtil();
		idApi.searchAPI("java+developer", null);
		String[] jobIds = {"17c50aa2b125273f", "d6148c4de83e8f41"};
		idApi.getJobAPI(jobIds);
	} */	

	
}
