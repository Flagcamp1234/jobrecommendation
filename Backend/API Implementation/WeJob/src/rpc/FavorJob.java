package rpc;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import db.DBConnection;
import db.DBConnectionFactory;
import entity.Job;

@WebServlet("/like")
public class FavorJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** 
	 * Favor a single job id
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	   		 throws ServletException, IOException { 
		DBConnection connection = DBConnectionFactory.getConnection();
	  	try {
	  		 JSONObject input = RpcHelper.readJSONObject(request);	  		 
	  		 
	  		 String username = input.getString("username");
	  		 JSONArray array = input.getJSONArray("like");
	  		 List<Job> jobs = JobHelper.getJobs(array);
	  		 for(Job job : jobs) {
	  			 connection.setSingleFavoriedItem(username, job);
	  		 }
	  		 RpcHelper.writeJsonObject(response, new JSONObject().put("result", "SUCCESS"));
	  		
	  	 } catch (Exception e) {
	  		 e.printStackTrace();
	  	 } finally {
	  		 connection.close();
	  	 }
	}
	/**
	 * Delete a single job id
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) {
		
//	    req = {
//	    	      user_id: user_id,
//	    	      like: [api_job_id]
//	    	    }

		DBConnection connection = DBConnectionFactory.getConnection();
	  	try {
	  		 JSONObject input = RpcHelper.readJSONObject(request);
	  		 String username = input.getString("username");
	  		 JSONArray array = input.getJSONArray("like");
	  		 for(int i = 0; i < array.length(); ++i) {
		  		 connection.removeSingleFavoredItem(username, array.getJSONObject(i).getString("api_job_id"));
	  		 }

	  		 RpcHelper.writeJsonObject(response, new JSONObject().put("result", "SUCCESS"));
	  		
	  	 } catch (Exception e) {
	  		 e.printStackTrace();
	  	 } finally {
	  		 connection.close();
	  	 }
	}
	
	/**
	 * Get liked job list from the user
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		String username = request.getParameter("username");
		JSONArray array = new JSONArray();
		String jobid = request.getParameter("jobid");
		DBConnection conn = DBConnectionFactory.getConnection();
		try {
			if(jobid == "" || jobid == null) {
				Set<Job> jobs = conn.getFavoriedItems(username);
				for (Job job : jobs) {
					JSONObject obj = job.toJSONObject();
					array.put(obj);
			}
		}
		else {
			Job job = conn.getSingleFavoriedItem(username, jobid);
			if (job != null) {
			JSONObject obj = job.toJSONObject();
			array.put(obj);
			}
		}
			
			RpcHelper.writeJsonArray(response, array);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
}
