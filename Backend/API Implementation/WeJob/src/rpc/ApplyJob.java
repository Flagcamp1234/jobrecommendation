package rpc;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import db.DBConnection;
import db.DBConnectionFactory;
import entity.Job;

@WebServlet("/apply")
public class ApplyJob extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 
	 * Apply a single job
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	   		 throws ServletException, IOException {	
		DBConnection connection = DBConnectionFactory.getConnection();
	  	try {
	  		 JSONObject input = RpcHelper.readJSONObject(request);
	  		 String username = input.getString("username");
	  		 JSONArray array = input.getJSONArray("apply");
	  		 List<Job> jobs = JobHelper.getJobs(array);
	  		 for(Job job : jobs) {
	  			 connection.setSingleAppliedItem(username, job);
	  		 }

	  		 RpcHelper.writeJsonObject(response, new JSONObject().put("result", "SUCCESS"));
	  		
	  	 } catch (Exception e) {
	  		 e.printStackTrace();
	  	 } finally {
	  		 connection.close();
	  	 }
	}
	
	
	/**
	 * Get single applied job for the user
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		String username = request.getParameter("username");
		String jobid = request.getParameter("jobid");
		JSONArray array = new JSONArray();
		
		DBConnection conn = DBConnectionFactory.getConnection();
		try {
			if(jobid == "" || jobid == null) {
					Set<Job> jobs = conn.getAppliedItems(username);
					for (Job job : jobs) {
						JSONObject obj = job.toJSONObject();
						array.put(obj);
				}
			}
			else {
				Job job = conn.getSingleAppliedItem(username, jobid);
				if (job != null) {
				JSONObject obj = job.toJSONObject();
				array.put(obj);
				}
			}
			RpcHelper.writeJsonArray(response, array);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
}
