package rpc;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import entity.Job;
import entity.Job.JobBuilder;

public class JobHelper {
	public static List<Job> getJobs(JSONArray jobs) throws JSONException{
//  	 [{
//	         api_job_id : api_job_id, 
//	         job_title : job_title,
//	         location : location,
//			 company  : company,
//	         job_snippet : job_snippet,
//	         job_url : job_url
//	      }]
		List<Job> jobList = new ArrayList<>();
		for(int i = 0; i < jobs.length(); i++) {
			JSONObject job = jobs.getJSONObject(i);
			JobBuilder builder = new JobBuilder();
			if(!job.isNull("api_job_id")) {
				builder.setApiJobId(job.getString("api_job_id"));
			}
			if(!job.isNull("job_title")) {
				builder.setJobTitle(job.getString("job_title"));
			}
			if(!job.isNull("location")) {
				builder.setLocation(job.getString("location"));
			}
			if (!job.isNull("company")) {
				builder.setCompany(job.getString("company"));
			}
			if (!job.isNull("job_snippet")) {
				builder.setJobSnippet(job.getString("job_snippet"));
			}
			if (!job.isNull("job_url")) {
				builder.setJobUrl(job.getString("job_url"));
			}
			
			jobList.add(builder.build());
		}
		
		return jobList;
	}
}
