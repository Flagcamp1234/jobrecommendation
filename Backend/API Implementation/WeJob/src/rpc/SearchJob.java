package rpc;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import entity.Job;
import util.SearchUtil;

@WebServlet("/search")
public class SearchJob extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/** 
	 * Search jobs
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	   		 throws ServletException, IOException {	
		doGet(request, response);
	}
	
	/**
	 * Search Job
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		String searchTerm = request.getParameter("searchTerm");
		String location = request.getParameter("location");
		String username = request.getParameter("username");
		SearchUtil idAPI = new SearchUtil();		

		try {
			List<Job> jobs = idAPI.search(searchTerm, location, username);
			
			JSONArray array = new JSONArray();
			for(Job job : jobs) {
				JSONObject obj = job.toJSONObject();
				array.put(obj);
			}
			RpcHelper.writeJsonArray(response, array);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

