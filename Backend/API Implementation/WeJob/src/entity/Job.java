package entity;

import org.json.JSONObject;

/**
 * Job Bean class
 *
 */
public class Job {
	private String apiJobId;
	private String jobTitle;
	private String company;
	private String location;
	private String jobSnippet;
	private String jobUrl;
	
	private Job(JobBuilder builder) {
		this.apiJobId = builder.apiJobId;
		this.jobTitle = builder.jobTitle;
		this.company = builder.company;
		this.location = builder.location;
		this.jobSnippet = builder.jobSnippet;
		this.jobUrl = builder.jobUrl;
	}
	
	public String getApiJobId() {
		return apiJobId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public String getCompany() {
		return company;
	}
	
	public String getLocation() {
		return location;
	}

	public String getJobSnippet() {
		return jobSnippet;
	}
	
	public String getJobUrl() {
		return jobUrl;
	}
	
	public JSONObject toJSONObject() {
		JSONObject object = new JSONObject();
		try {
			object.put("api_job_id", apiJobId);
			object.put("job_title", jobTitle);
			object.put("company", company);
			object.put("location", location);
			object.put("job_snippet", jobSnippet);
			object.put("job_url", jobUrl);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return object;
	}

	public static class JobBuilder {
		private String apiJobId;
		private String jobTitle;
		private String company;
		private String location;
		private String jobSnippet;
		private String jobUrl;
		
		public JobBuilder setApiJobId(String apiJobId) {
			this.apiJobId = apiJobId;
			return this;
		}
		
		public JobBuilder setJobTitle(String jobTitle) {
			this.jobTitle = jobTitle;
			return this;
		}
		public JobBuilder setCompany(String company) {
			this.company = company;
			return this;
		}
		public JobBuilder setLocation(String location) {
			this.location = location;
			return this;
		}
		public JobBuilder setJobSnippet(String jobSnippet) {
			this.jobSnippet = jobSnippet;
			return this;
		}			
		public JobBuilder setJobUrl(String jobUrl) {
			this.jobUrl = jobUrl;
			return this;
		}
		public Job build() {
			return new Job(this);
		}
	}
}
