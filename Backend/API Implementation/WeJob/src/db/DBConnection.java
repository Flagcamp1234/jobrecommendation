package db;

import java.util.Set;
import entity.Job;
import entity.User;

public interface DBConnection extends AutoCloseable {
	 
	/* Close the connection.
	 */
	public void close();

	/**
	 * register a user
	 * @param username
	 * @param password
	 * @param emailAddress
	 * @return
	 */
	public boolean createUserProfile(String username, String password, String email);
	
	/**
	 * 
	 * @param username
	 * @return
	 */
	public User getUserProfile(String username, String password);
	
	/**
	 * 
	 * @param username
	 * @param jobId
	 * @return
	 */
	public void setSingleAppliedItem(String username, Job job);
	
	/**
	 * 
	 * @param username
	 * @param jobId
	 * @return
	 */
	public Set<Job> getAppliedItems(String username);
	
	
	public Job getSingleAppliedItem(String username, String jobid);
	
	/**
	 * 
	 * @param username
	 * @return
	 */
	
	public void setSingleFavoriedItem(String username, Job job) ;
	
	/**
	 * 
	 * @param username
	 * @param job
	 * @return
	 */
	public Job getSingleFavoriedItem(String username, String jobid);
	/**
	 * 
	 * @param username
	 * @return
	 */
	public Set<Job> getFavoriedItems(String username);
	
	/**
	 * 
	 * @param username
	 * @param jobId
	 * @return
	 */


	public void removeSingleFavoredItem(String username, String api_job_id);
	
}

