package db.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import db.DBConnection;
import entity.Job;
import entity.Job.JobBuilder;
import entity.User.UserBuilder;
import entity.User;

public class MySQLConnection implements DBConnection {
   private Connection conn;
   
   public MySQLConnection() {
  	 try {
  		Class.forName("com.mysql.cj.jdbc.Driver").getConstructor().newInstance();
  		 conn = DriverManager.getConnection(MySQLDBUtil.URL);
  		
  	 } catch (Exception e) {
  		 e.printStackTrace();
  	 }
   }

	@Override
	public void close() {
	  	 if (conn != null) {
	  		 try {
	  			 conn.close();
	  		 } catch (Exception e) {
	  			 e.printStackTrace();
	  		 }
	  	 }
	}

	@Override
	public boolean createUserProfile(String username, String password, String email) {
		if (conn == null) {
			System.err.println("DB connection failed");
			return false;
		}

		try {

			String sql = "INSERT IGNORE INTO users (username, password, email) VALUES (?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ps.setString(3, email);
			
			return ps.executeUpdate() == 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;	
	}	


	@Override
	public User getUserProfile(String username, String password) {
		if (conn == null) {
			return null;
		}
		
		User this_user = null;
		UserBuilder builder = new UserBuilder();
		
		try {
			String sql = "SELECT email FROM users WHERE username = ? and password = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1,  username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			
			if (!rs.first()) {
				return this_user;
			}
			while (rs.next()) {				
				builder.setEmailAddress(rs.getString("email"));
							
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this_user = builder.setPassword(password).setUserName(username).build();
		return this_user;		
	}


	@Override
	public void setSingleAppliedItem(String username, Job job) {
        if (conn == null) {
	  		 System.err.println("DB connection failed");
	  		 return;
  	       }
  	
  	     try {
  	    	String api_job_id = job.getApiJobId();
  	    	String job_title = job.getJobTitle();
			String company_name = job.getCompany();
			String location = job.getLocation();
			String job_snippet = job.getJobSnippet();
			String job_url = job.getJobUrl();
														 
	  		String sql = "INSERT IGNORE INTO job_applied (api_job_id, username, job_title, company_name, location, job_snippet, job_url) VALUES (?,?,?,?,?,?,?)";
	  		PreparedStatement ps = conn.prepareStatement(sql);
	  		ps.setString(1, api_job_id);
	  		ps.setString(2, username);
	  		ps.setString(3, job_title);
	  		ps.setString(4, company_name);
	  		ps.setString(5, location);
	  		ps.setString(6, job_snippet);
	  		ps.setString(7, job_url);
	  		ps.execute();
  		 
  	     } catch (Exception e) {
  	    	 e.printStackTrace();
  	     }
	}

	@Override
	public Set<Job> getAppliedItems(String username) {
		if (conn == null) {
			return new HashSet<>();
		}
		Set<Job> appliedItem = new HashSet<>();
		
		try {
								
			// get user's applied job
			String sql = "SELECT api_job_id, job_title, company_name, location, job_snippet, job_url FROM job_applied WHERE username = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1,  username);
			ResultSet rs = ps.executeQuery();
			
			JobBuilder builder = new JobBuilder();
			while (rs.next()) {
				builder.setApiJobId(rs.getString("api_job_id"));
				builder.setJobTitle(rs.getString("job_title"));
				builder.setCompany(rs.getString("company_name"));
				builder.setLocation(rs.getString("location"));
				builder.setJobSnippet(rs.getString("job_snippet"));
				builder.setJobUrl(rs.getString("job_url"));
				
				appliedItem.add(builder.build());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return appliedItem;		
	}
	
	@Override
	public Job getSingleAppliedItem(String username, String job_id) {
		Job result = null;
		if (conn == null) {
			return result;
		}
				
		try {
								
			// get user's applied job
			String sql = "SELECT api_job_id, job_title, company_name, location, job_snippet, job_url FROM job_applied WHERE username = ? and api_job_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1,  username);
			ps.setString(2, job_id);
			ResultSet rs = ps.executeQuery();
			
			JobBuilder builder = new JobBuilder();
			while (rs.next()) {
				builder.setApiJobId(rs.getString("api_job_id"));
				builder.setJobTitle(rs.getString("job_title"));
				builder.setCompany(rs.getString("company_name"));
				builder.setLocation(rs.getString("location"));
				builder.setJobSnippet(rs.getString("job_snippet"));
				builder.setJobUrl(rs.getString("job_url"));				
				result = builder.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;		
	}


	@Override
	public void setSingleFavoriedItem(String username, Job job)  {
	       if (conn == null) {
		  		 System.err.println("DB connection failed");
		  		 return;
	  	       }
	  	
	  	     try {
	  	    	String api_job_id = job.getApiJobId();
	  	    	String job_title = job.getJobTitle();
				String company_name = job.getCompany();
				String location = job.getLocation();
				String job_snippet = job.getJobSnippet();
				String job_url = job.getJobUrl();
																	
		        // insert record into job_liked
		  		String sql = "INSERT IGNORE INTO job_liked (api_job_id, username, job_title, company_name, location, job_snippet, job_url) VALUES (?,?,?,?,?,?,?)";
		  		PreparedStatement ps = conn.prepareStatement(sql);
		  		ps.setString(1, api_job_id);
		  		ps.setString(2, username);
		  		ps.setString(3, job_title);
		  		ps.setString(4, company_name);
		  		ps.setString(5, location);
		  		ps.setString(6, job_snippet);
		  		ps.setString(7, job_url);
		  		ps.execute();
	  		 
	  	     } catch (Exception e) {
	  	    	 e.printStackTrace();
	  	     }
	}
	
	@Override
	public Job getSingleFavoriedItem(String username, String job_id) {
		Job result = null;
		if (conn == null) {
			return result;
		}
				
		try {
								
			// get user's applied job
			String sql = "SELECT api_job_id, job_title, company_name, location, job_snippet, job_url FROM job_liked WHERE username = ? and api_job_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1,  username);
			ps.setString(2, job_id);
			ResultSet rs = ps.executeQuery();
			
			JobBuilder builder = new JobBuilder();
			while (rs.next()) {
				builder.setApiJobId(rs.getString("api_job_id"));
				builder.setJobTitle(rs.getString("job_title"));
				builder.setCompany(rs.getString("company_name"));
				builder.setLocation(rs.getString("location"));
				builder.setJobSnippet(rs.getString("job_snippet"));
				builder.setJobUrl(rs.getString("job_url"));				
				result = builder.build();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;		
	}
	@Override
	public Set<Job> getFavoriedItems(String username) {
		if (conn == null) {
			return new HashSet<>();
		}
		Set<Job> appliedItem = new HashSet<>();
		
		try {
				
			// get user's liked job
			String sql = "SELECT api_job_id, job_title, company_name, location, job_snippet, job_url FROM job_liked WHERE username = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1,  username);
			ResultSet rs = ps.executeQuery();
			
			JobBuilder builder = new JobBuilder();
			while (rs.next()) {
				builder.setApiJobId(rs.getString("api_job_id"));
				builder.setJobTitle(rs.getString("job_title"));
				builder.setCompany(rs.getString("company_name"));
				builder.setLocation(rs.getString("location"));
				builder.setJobSnippet(rs.getString("job_snippet"));
				builder.setJobUrl(rs.getString("job_url"));
				
				appliedItem.add(builder.build());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return appliedItem;		
	}

	@Override
	public void removeSingleFavoredItem(String username, String api_job_id) {
        if (conn == null) {
 		 System.err.println("DB connection failed");
 		 return;
 	     }
 	
 	     try { 				  	  	    	 
	  		String sql = "DELETE FROM job_liked WHERE username = ? AND api_job_id = ?";
	  		PreparedStatement ps = conn.prepareStatement(sql);
	  		ps.setString(1, username);
	  		ps.setString(2, api_job_id);
	  		ps.execute();
	  		 
 		
 	       } catch (Exception e) {
 	    	   e.printStackTrace();
 	       }
	}



}
//test