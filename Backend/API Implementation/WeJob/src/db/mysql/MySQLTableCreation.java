package db.mysql;

import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Connection;

public class MySQLTableCreation {
	// Run this as Java application to reset db schema.
	public static void main(String[] args) {
		try {
			// Step 1 Connect to MySQL.
			System.out.println("Connecting to " + MySQLDBUtil.URL);
			Class.forName("com.mysql.cj.jdbc.Driver").getConstructor().newInstance();
			Connection conn = DriverManager.getConnection(MySQLDBUtil.URL);
			
			if (conn == null) {
				return;
			}			 	  
			
			// Step 2 Drop tables in case they exist.
			Statement statement = conn.createStatement();			
			
			
			String sql = "DROP TABLE IF EXISTS job_liked";
			statement.executeUpdate(sql);
			
			sql = "DROP TABLE IF EXISTS job_applied";
			statement.executeUpdate(sql);
			
			sql = "DROP TABLE IF EXISTS users";
			statement.executeUpdate(sql);
			// Step 3 Create new tables
			sql = "CREATE TABLE IF NOT EXISTS users ("
					+ "user_id int NOT NULL AUTO_INCREMENT,"
					+ "username VARCHAR(255) NOT NULL,"
					+ "password VARCHAR(255) NOT NULL,"
					+ "email VARCHAR(255) NOT NULL,"
					+ "PRIMARY KEY (user_id),"
					+ "UNIQUE (username),"
					+ "UNIQUE (email)"
					+ ")";
			statement.executeUpdate(sql);			

			sql = "CREATE TABLE IF NOT EXISTS job_applied ("
					+ "job_applied_id int NOT NULL AUTO_INCREMENT,"
					+ "api_job_id VARCHAR(255) NOT NULL,"
					+ "username VARCHAR(255) NOT NULL,"
					+ "job_title VARCHAR(255) NOT NULL,"
					+ "company_name VARCHAR(255) NOT NULL,"
					+ "location VARCHAR(255) NOT NULL,"
					+ "job_snippet VARCHAR(1000) NULL,"
					+ "job_url VARCHAR(300) NOT NULL,"
					+ "PRIMARY KEY (job_applied_id),"
					+ "FOREIGN KEY (username) REFERENCES users(username),"
					+ "CONSTRAINT api_job_id_user_name UNIQUE (api_job_id, username)"
					+ ")";
			statement.executeUpdate(sql);
			
			sql = "CREATE TABLE IF NOT EXISTS job_liked ("
					+ "job_liked_id int NOT NULL AUTO_INCREMENT,"
					+ "api_job_id VARCHAR(255) NOT NULL,"
					+ "username VARCHAR(255) NOT NULL,"
					+ "job_title VARCHAR(255) NOT NULL,"
					+ "company_name VARCHAR(255) NOT NULL,"
					+ "location VARCHAR(255) NOT NULL,"
					+ "job_snippet VARCHAR(1000) NULL,"
					+ "job_url VARCHAR(300) NOT NULL,"
					+ "PRIMARY KEY (job_liked_id),"
					+ "FOREIGN KEY (username) REFERENCES users(username),"
					+ "CONSTRAINT api_job_id_user_name UNIQUE (api_job_id, username)"
					+ ")";
			statement.executeUpdate(sql);
			
			// Step 4: insert fake user 1111/3229c1097c00d497a0fd282d586be050
						sql = "INSERT INTO users(username, password, email) VALUES('1111', '3229c1097c00d497a0fd282d586be050', 'johnsmith@gmail.com')";
						statement.executeUpdate(sql);

						// fake job liked
						sql = "INSERT INTO job_liked(username, api_job_id, job_title,company_name,location,job_snippet,job_url) VALUES('1111', 'b54c8d06449a5172', 'Application Engineer', 'Google','Austin, TX 78731','Programming experience in <b>Java</b>, Spring, Hibernate, Web Services (RESTful, SOAP), JavaScript. When the situation calls for it, you�ll be part of a team that...','http://www.indeed.com/viewjob?jk=b54c8d06449a5172&qd=DwH5aUFcigGJUdalCG1k4QMPJIcFSlm87e21Z1cw67qQM8Fp4swq5uKehVNPma9rTpxA_LZts4K3mkt3poQiWpPfDLpee6FYH-bY6rapYf0&indpubnum=125768650773419&atk=1da5bu4bg4trh800')";
						statement.executeUpdate(sql);
						// fake job applied
						sql = "INSERT INTO job_applied(username, api_job_id, job_title,company_name,location,job_snippet,job_url) VALUES('1111', 'b54c8d06449a5172', 'Application Engineer', 'Google','Austin, TX 78731','Programming experience in <b>Java</b>, Spring, Hibernate, Web Services (RESTful, SOAP), JavaScript. When the situation calls for it, you�ll be part of a team that...','http://www.indeed.com/viewjob?jk=b54c8d06449a5172&qd=DwH5aUFcigGJUdalCG1k4QMPJIcFSlm87e21Z1cw67qQM8Fp4swq5uKehVNPma9rTpxA_LZts4K3mkt3poQiWpPfDLpee6FYH-bY6rapYf0&indpubnum=125768650773419&atk=1da5bu4bg4trh800')";
						statement.executeUpdate(sql);						
						
			conn.close();
			
			System.out.println("Import done successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

