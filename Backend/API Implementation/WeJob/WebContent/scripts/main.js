(function() {

  /**
   * Variables
   */
  var username = 'John';
  /**
   * Initialize major event handlers
   */
  function init() {
    // register event listeners
    document.querySelector('#login-btn').addEventListener('click', login);
    document.querySelector('#search-btn').addEventListener('click',loadSearchPage);
    document.querySelector('#fav-btn').addEventListener('click', loadFavoriteItems);
    document.querySelector('#apply-btn').addEventListener('click', loadAppliedItems);
    document.querySelector('#recommend-btn').addEventListener('click', loadRecommendedItems);
    document.querySelector('#signup-btn').addEventListener('click', signup);
    document.querySelector('#register-btn').addEventListener('click', register);
    document.querySelector('#regiesterlogin-btn').addEventListener('click', loginPage);
    document.querySelector('#findjob').addEventListener('click', searchJob);
    document.querySelector('#useLocation').addEventListener('click', useLocationForRec);
    document.querySelector('#useLikedJobs').addEventListener('click', useLikedJobForRec);
    document.querySelector('#modal-content-close').addEventListener('click', hideJobDetails);
    validateSession();
    //startCORSServer();
    // onSessionValid({"username":"1111","name":"John Smith","status":"OK"});
  }
  
  /**
   * Session
   */
  function validateSession() {
    onSessionInvalid();
    // The request parameters
    var url = './login';
    var req = JSON.stringify({});
    // make AJAX call
    ajax('GET', url, req,
      // session is still valid
      function(res) {
        var result = JSON.parse(res);

        if (result.status === 'OK') {
          onSessionValid(result);
        }
      });
  }

  function onSessionValid(result) {
	username = result.username;
    var loginForm = document.querySelector('#login-form');
    var signupForm = document.querySelector('#signup-form');
    var itemNav = document.querySelector('#item-nav');
    var itemList = document.querySelector('#item-list-detail');
    var avatar = document.querySelector('#avatar');
    var welcomeMsg = document.querySelector('#welcome-msg');
    var logoutBtn = document.querySelector('#logout-link');
    var searchbar = document.querySelector('#searchbar');
    var location = document.querySelector('#where');
    var recOptions = document.querySelector('#recommend-option');
    hideElement(recOptions);
    setSearchDefaultLocation();
    welcomeMsg.innerHTML = 'Welcome, ' + username;
    showElementwithClass(searchbar, 'search-bar')
    showElement(itemNav);
    showElementwithClass(itemList, 'item-list-search');
    showElement(avatar);
    showElement(welcomeMsg);
    showElement(logoutBtn, 'inline-block');
    showElement(searchbar)
    hideElement(loginForm);
    hideElement(signupForm);
    document.title = "Job Search|WebJob";
  }

  function onSessionInvalid() {
	var signupForm = document.querySelector('#signup-form');
    var loginForm = document.querySelector('#login-form');
    var itemNav = document.querySelector('#item-nav');
    var itemList = document.querySelector('#item-list-detail');
    var avatar = document.querySelector('#avatar');
    var welcomeMsg = document.querySelector('#welcome-msg');
    var logoutBtn = document.querySelector('#logout-link');
    var message = document.querySelector('#message');
    var searchbar = document.querySelector('#searchbar');
    var recOptions = document.querySelector('#recommend-option');
    hideElement(recOptions);
    hideElement(searchbar);
    hideElement(itemNav);
    hideElement(itemList);
    hideElement(avatar);
    hideElement(logoutBtn);
    hideElement(welcomeMsg);
    hideElement(signupForm);
    showElement(loginForm);
    showElement(message);
  }
  
  function loginPage() {
	  var signupForm = document.querySelector('#signup-form');
	    var loginForm = document.querySelector('#login-form');
	    var itemNav = document.querySelector('#item-nav');
	    var itemList = document.querySelector('#item-list-detail');
	    var avatar = document.querySelector('#avatar');
	    var welcomeMsg = document.querySelector('#welcome-msg');
	    var logoutBtn = document.querySelector('#logout-link');
	    var searchbar = document.querySelector('#searchbar');
	    hideElement(searchbar);
	    hideElement(itemNav);
	    hideElement(itemList);
	    hideElement(avatar);
	    hideElement(logoutBtn);
	    hideElement(welcomeMsg);
	    hideElement(signupForm);
	   
	    showElement(loginForm);	  
	    
  }

  function hideElement(element) {
    element.style.display = 'none';
  }

  function showElement(element, style) {
    var displayStyle = style ? style : 'block';
    element.style.display = displayStyle;
  }
  
  function showElementwithClass(element, classname) {	 
	  element.classList.add(classname);
	  element.style.display = 'block';
  }

  

  // -----------------------------------
  // Login
  // -----------------------------------

  function login() {
	clearLoginError();

    username = document.querySelector('#username').value;
    var password = document.querySelector('#password').value;
	if(username === "" || password === "") {
    	allrequiredFieldsForLogin();
    	return;
    }
    password = md5(username + md5(password));

    // The request parameters
    var url = './login';
    var req = JSON.stringify({
      username : username,
      password : password,
    });

    ajax('POST', url, req,
      // successful callback
      function(res) {
        var result = JSON.parse(res);

        // successfully logged in
        if (result.status === 'OK') {
          onSessionValid(result);
        }
      },

      // error
      function() {
        showLoginError();
      },
      true);
  }
 // register button click will show the signup block
function register() {
	var signupForm = document.querySelector('#signup-form');
    var loginForm = document.querySelector('#login-form');
    var itemNav = document.querySelector('#item-nav');
    var itemList = document.querySelector('#item-list-detail');
    var avatar = document.querySelector('#avatar');
    var welcomeMsg = document.querySelector('#welcome-msg');
    var logoutBtn = document.querySelector('#logout-link');
    var message = document.querySelector('#message');
    var searchbar = document.querySelector('#searchbar');
    var recOptions = document.querySelector('#recommend-option');
    hideElement(recOptions);
    hideElement(searchbar);
    hideElement(itemNav);
    hideElement(itemList);
    hideElement(avatar);
    hideElement(logoutBtn);
    hideElement(welcomeMsg);
    hideElement(loginForm);
    showElement(signupForm);
    showElement(message);
    //showSignupError();
}

function onSignUpSuccessful() {
	var signupForm = document.querySelector('#signup-form');
    var loginForm = document.querySelector('#login-form');
    var itemNav = document.querySelector('#item-nav');
    var itemList = document.querySelector('#item-list-detail');
    var avatar = document.querySelector('#avatar');
    var welcomeMsg = document.querySelector('#welcome-msg');
    var logoutBtn = document.querySelector('#logout-link');
    var message = document.querySelector('#message');
    var searchbar = document.querySelector('#searchbar');
    var recOptions = document.querySelector('#recommend-option');
    hideElement(recOptions);
    hideElement(searchbar);
    showElement(message);
    hideElement(itemNav);
    hideElement(itemList);
    hideElement(avatar);
    hideElement(logoutBtn);
    hideElement(welcomeMsg);
    showElement(loginForm);
    hideElement(signupForm);
    showSignupSuccessful();
}

function onSignUpFailed() {
	var signupForm = document.querySelector('#signup-form');
    var loginForm = document.querySelector('#login-form');
    var itemNav = document.querySelector('#item-nav');
    var itemList = document.querySelector('#item-list-detail');
    var avatar = document.querySelector('#avatar');
    var welcomeMsg = document.querySelector('#welcome-msg');
    var logoutBtn = document.querySelector('#logout-link'); 
    var searchbar = document.querySelector('#searchbar');
    var recOptions = document.querySelector('#recommend-option');
    hideElement(recOptions);
    hideElement(searchbar);
    hideElement(itemNav);
    hideElement(itemList);
    hideElement(avatar);
    hideElement(logoutBtn);
    hideElement(welcomeMsg);
    hideElement(loginForm);
    showElement(signupForm);
    showSignupError();
}

  function signup() {
	  clearSignUpMsg();
	   
	    username = document.querySelector('#registerusername').value;
	    var password = document.querySelector('#registerpassword').value;
	    var email = document.querySelector('#registeremail').value;	    
	    if (username === "" || password === "" || email === "") {
	    		if (username == "") {
	    			document.querySelector('#registerusername').focus();
	    		}
	    		if (password === "") {
	    			document.querySelector('#registerpassword').focus();
	    		}
	    		if (email === "") {
	    			document.querySelector('#registeremail').focus();
	    		}
		    	allrequiredFieldsForSignUp();
		    	return;
		    }
	    if (validatePassword(password) === false) {
	    	document.querySelector('#registerpassword').focus();
	    	return;
	    }
        if (validateEmail(email) === false) {
        	document.querySelector('#registeremail').focus();
        	return;
        }
	    password = md5(username + md5(password));

	    // The request parameters
	    var url = './register';
	    var req = JSON.stringify({
	      username : username,
	      password : password,
	      email: email
	    });

	    ajax('POST', url, req,
	      // successful callback
	      function(res) {
	        var result = JSON.parse(res);

	        // successfully logged in
	        if (result.status === 'OK') {
	            onSignUpSuccessful(result);
	        }
	        else {
	        	onSignUpFailed(result);
	        }
	      },

	      // error
	      function() {
	    	  showSignupError();
	      },
	      true);
	  }
  function validatePassword(password)
  {
	 var pwdPattern = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/g;
	 if (password.match(pwdPattern) === null) {
		 passwordInvalidFormat(password);
		 return false;
	 }
	 return true;
  }
  
  function validateEmail(email) {
	  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/g;
	  if (email.match(mailformat) === null) {
		  emailInvalidFormat(email);
		  return false;
	  }
	  return true;
  }
  function showLoginError() {
	  var el = document.querySelector('#loginmessage');
	    if (el.classList.contains("successful-msg")) {
			  el.classList.remove("successful-msg")
		  }
	el.innerHTML = 'Invalid username or password.';
	el.classList.add("error-msg");
  }
  
  function allrequiredFieldsForLogin() {
	    var el = document.querySelector('#loginmessage');
	    el.innerHTML = 'All fields for login are required.';
	    if (el.classList.contains("successful-msg")) {
			  el.classList.remove("successful-msg")
		  }
	    el.classList.add("error-msg");
	  }

  function clearLoginError() {
    document.querySelector('#loginmessage').innerHTML = '';
  }
  
  function showSignupError() {
	  var el = document.querySelector('#signupmessage');
	  if (el.classList.contains("successful-msg")) {
		  el.classList.remove("successful-msg")
	  }
	  el.classList.add("error-msg"); 
	  el.innerHTML += '<p>The username or the email is already used.</p>'
  }
  
  function showSignupSuccessful() {
	  var el =  document.querySelector('#loginmessage');
	  if (el.classList.contains("error-msg")) {
		  el.classList.remove("error-msg")
	  }
	  el.classList.add("successful-msg"); 
	  el.innerHTML = 'Registered successfully.'
}
  function allrequiredFieldsForSignUp() {
	  var el = document.querySelector('#signupmessage');
	  if (el.classList.contains("successful-msg")) {
		  el.classList.remove("successful-msg")
	  }
	  el.classList.add("error-msg");
	  el.innerHTML += 'All fields for sign up are required.';	 
  }
  
  function emailInvalidFormat() {
	  var el = document.querySelector('#signupmessage');
	  if (el.classList.contains("successful-msg")) {
		  el.classList.remove("successful-msg")
	  }
	  el.classList.add("error-msg");
	  el.innerHTML += '<p>Email Address is in wrong format.</p>';
  }
  
  function passwordInvalidFormat() {
	  var el = document.querySelector('#signupmessage');
	  if (el.classList.contains("successful-msg")) {
		  el.classList.remove("successful-msg")
	  }
	  el.classList.add("error-msg");
	  el.innerHTML += '<p>Password must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters</p>';
  }
  
  function clearSignUpMsg() {
	    document.querySelector('#signupmessage').innerHTML = '';
	  }

  
  // -----------------------------------
  // Helper Functions
  // -----------------------------------

  /**
   * A helper function that makes a navigation button active
   *
   * @param btnId - The id of the navigation button
   */
  function activeBtn(btnId) {
    var btns = document.querySelectorAll('.main-nav-btn');

    // deactivate all navigation buttons
    for (var i = 0; i < btns.length; i++) {
      btns[i].className = btns[i].className.replace(/\bactive\b/, '');
    }

    // active the one that has id = btnId
    var btn = document.querySelector('#' + btnId);
    btn.className += ' active';
  }

  function showLoadingMessage(msg) {
    var itemList = document.querySelector('#item-list-detail');
    itemList.innerHTML = '<p class="notice"><i class="fa fa-spinner fa-spin"></i> ' +
      msg + '</p>';
  }

  function showWarningMessage(msg) {
    var itemList = document.querySelector('#item-list-detail');
    itemList.innerHTML = '<p class="notice"><i class="fa fa-exclamation-triangle"></i> ' +
      msg + '</p>';
  }

  function showErrorMessage(msg) {
    var itemList = document.querySelector('#item-list-detail');
    itemList.innerHTML = '<p class="notice"><i class="fa fa-exclamation-circle"></i> ' +
      msg + '</p>';
  }

  /**
   * A helper function that creates a DOM element <tag options...>
   * @param tag
   * @param options
   * @returns {Element}
   */
  function $create(tag, options) {
    var element = document.createElement(tag);
    for (var key in options) {
      if (options.hasOwnProperty(key)) {
        element[key] = options[key];
      }
    }
    return element;
  }

  /**
   * AJAX helper
   *
   * @param method - GET|POST|PUT|DELETE
   * @param url - API end point
   * @param data - request payload data
   * @param successCallback - Successful callback function
   * @param errorCallback - Error callback function
   */
  function ajax(method, url, data, successCallback, errorCallback) {
    var xhr = new XMLHttpRequest();

    xhr.open(method, url, true);

    xhr.onload = function() {
      if (xhr.status === 200 || xhr.status === 0) {
        successCallback(xhr.responseText);
      } else {
        errorCallback();
      }
    };

    xhr.onerror = function() {
      console.error("The request couldn't be completed.");
      errorCallback();
    };

    if (data === null) {
      xhr.send();
    } else {
      xhr.setRequestHeader("Content-Type",
        "application/json;charset=utf-8");
      xhr.send(data);
    }
  }

  // -------------------------------------
  // AJAX call server-side APIs
  // -------------------------------------
  function searchJob() {
		 var what =  document.querySelector('#what').value;
		 var where =  document.querySelector('#where').value;
		 if (what === '' && where === '') {
			 return; //do not do anything if the user did not fill anything, like google search.
		 }
		 // if (where === undefined || where === null || where === '') {
		//	 where = setSearchDefaultLocation();
		// }
		 searchbyItem(what, where, username);		 
	  }
  
	  function setSearchDefaultLocation() {
	    // get location from http://ipinfo.io/json
		  
		/**
		 * {
	  "ip": "107.140.89.46",
	  "hostname": "107-140-89-46.lightspeed.irvnca.sbcglobal.net",
	  "city": "Gardena",
	  "region": "California",
	  "country": "US",
	  "loc": "33.8951,-118.2910",
	  "postal": "90247",
	  "org": "AS7018 AT&T Services, Inc."
	}
		 */
	    var url = 'https://geoip-db.com/json/'
	    var data = null;
	    $.ajax({
            url: "https://geoip-db.com/jsonp",
            jsonpCallback: "callback",
            dataType: "jsonp",
            success: function( location ) {
            	console.log(location);
            	document.querySelector('#where').value = location.city + ', ' + location.state; 
            }
        });    
	  }
  /**
   * API #1 Load the nearby items API end point: [GET]
   * /search?searchTerm=software engineer&location=san jose, ca
   */
  function searchbyItem(searchTerm, location, username) {
    console.log('search item begins');
    activeBtn('search-btn');

    // The request parameters
    var url = './search';
    //var params = 'username=' + username + '&lat=' + lat + '&lon=' + lng;
    var params = 'searchTerm=' + searchTerm + '&location=' + location + '&username=' + username;
    var data = null;

    // make AJAX call
    ajax('GET', url + '?' + params, data,
      // successful callback
      function(res) {
        var items = JSON.parse(res);
        if (!items || items.length === 0) {
          showWarningMessage('No nearby item.');
        } else {
          listItems(items);
        }
      },
      // failed callback
      function() {
        showErrorMessage('Cannot load nearby items.');
      }
    );
  }
   function loadSearchPage() {
	   var searchbar = document.querySelector('#searchbar'); 
	   hideElement(searchbar);
	   showElementwithClass(searchbar, 'search-bar')
		var recOptions = document.querySelector('#recommend-option');
		hideElement(recOptions);
		var itemlist = document.querySelector('#item-list-detail');
		showElementwithClass(itemlist, 'item-list')
		itemlist.innerHTML = "";
		activeBtn('search-btn');
   }
  /**
   * API #2 Load favorite items API end point: [GET]
   * /like?username=2222
   */
  function loadFavoriteItems() {
	var searchbar = document.querySelector('#searchbar');
	hideElement(searchbar);
	 var recOptions = document.querySelector('#recommend-option');
	    hideElement(recOptions);
	var itemlist = document.querySelector('#item-list-detail');
	showElementwithClass(itemlist, 'item-list')   
    activeBtn('fav-btn');    
    // request parameters
    var url = './like';
    var params = 'username=' + username;
    var req = JSON.stringify({});

    // display loading message
    showLoadingMessage('Loading favorite items...');

    // make AJAX call
    ajax('GET', url + '?' + params, req, function(res) {
      var items = JSON.parse(res);
      if (!items || items.length === 0) {
        showWarningMessage('No favorite item.');
      } else {
        listItems(items);
      }
    }, function() {
      showErrorMessage('Cannot load favorite items.');
    });
  }
  /**
   * API #3 Load applied items API end point: [GET]
   * /apply?username=2222
   */
  function loadAppliedItems() {
	  var searchbar = document.querySelector('#searchbar');
		hideElement(searchbar);
		 var recOptions = document.querySelector('#recommend-option');
		    hideElement(recOptions);
		var itemlist = document.querySelector('#item-list-detail');
		showElementwithClass(itemlist, 'item-list')  
	    
	    activeBtn('apply-btn');
	    // request parameters
	    var url = './apply';
	    var params = 'username=' + username;
	    var req = JSON.stringify({});

	    // display loading message
	    showLoadingMessage('Loading Liked items...');

	    // make AJAX call
	    ajax('GET', url + '?' + params, req, function(res) {
	      var items = JSON.parse(res);
	      if (!items || items.length === 0) {
	        showWarningMessage('No applied item.');
	      } else {
	        listItems(items);
	      }
	    }, function() {
	      showErrorMessage('Cannot load applied items.');
	    });
	  }

  /**
   * API #4 Load recommended items API end point: [GET]
   * /recommend?username=1111&recWeight=0.6&userLocation=false
   */
  function useLocationForRec() {
	  loadRecommendedItems();
  }
  
  function useLikedJobForRec() {
	  loadRecommendedItems();
  }
  
  function loadRecommendedItems() {
	  var searchbar = document.querySelector('#searchbar');
	  var useLikedJobs = document.querySelector('#useLikedJobs');
	  var useLocation = document.querySelector('#useLocation');
		hideElement(searchbar);
	  var recOptions = document.querySelector('#recommend-option');
	  showElementwithClass(recOptions, 'recsection'); 
		var itemlist = document.querySelector('#item-list-detail');
		showElementwithClass(itemlist, 'item-list-search'); 
		var recWeight = useLikedJobs.checked == false ? '0.4' :'0.6'; var useLoc = useLocation.checked == false ? 'false':'true';
		activeBtn('recommend-btn');
		recommendJobWithOptions(useLoc, recWeight) 
  }
  
  function recommendJobWithOptions(useLocation, recWeight) {
	  if (recWeight === null || recWeight === undefined) {
	    	recWeight = '0.6';
	    }
	    if (useLocation === null || useLocation === undefined) {
	    	useLocation = 'false';
	    }
	    // request parameters
	    var url = './recommend' + '?' + 'username=' + username + '&recWeight=' + recWeight + '&useLocation=' + useLocation;
	    var data = null;

	    // display loading message
	    showLoadingMessage('Loading recommended items...');

	    // make AJAX call
	    ajax('GET', url, data,
	      // successful callback
	      function(res) {
	        var items = JSON.parse(res);
	        if (!items || items.length === 0) {
	          showWarningMessage('No recommended item. Make sure you have favorites and/or applied items.');
	        } else {
	          listItems(items);
	        }
	      },
	      // failed callback
	      function() {
	        showErrorMessage('Cannot load recommended items.');
	      }
	    );	  
  }

  /**
   * API #4 dislike a single items
   *
   * @param item
   *
   *
   */
  function disFavorItem(item) {
	  var url = './like';	    
	    var method = 'DELETE';
	    var object = {
		        "username": username,
		        "like": [{
		            "api_job_id" : item.api_job_id 				            
		         }]
		      }
	    var req = JSON.stringify(object);
	    ajax(method, url, req,
			      // successful callback
			      function(res) {
			        var result = JSON.parse(res);
			        if (result.status === 'OK' || result.result === 'SUCCESS') {			        	
			        	$('#job-like').text('Like');
			        	$('#job-like').removeClass('modal-content-liked').addClass('modal-content-like');
			        	$('#job-like').off("click");
			        	$('#job-like').click(function() {favorItem(item)});				        	
			        }
			      });
  }
  
  /**
   *  API #5 like a single item
   * 
   * * @param item
   */
  function favorItem(item) {
	  if (item !== undefined && item !== null) {
	  var url = './like';	    
	    var method = 'POST';
	    var object = {
		        "username": username,
		        "like": [{
		            "api_job_id" : item.api_job_id, 
		            "job_title" : item.job_title,
		            "location" : item.location,
		   		 	"company"  : item.company,
		            "job_snippet" : item.job_snippet,
		   		 	"job_url" : item.job_url
		         }]
		      }
	    var req = JSON.stringify(object);
	    ajax(method, url, req,
	      // successful callback
	      function(res) {
	        var result = JSON.parse(res);
	        if (result.status === 'OK' || result.result === 'SUCCESS') {
	        	$('#job-like').text('Unlike');
	        	$('#job-like').removeClass('modal-content-like').addClass('modal-content-liked');        	
	        	$('#job-like').off("click");
	        	$('#job-like').click(function() {disFavorItem(item)});
	        }
	      });  
  }
	  }
  /**
   *  API #6 Apply a single item
   *  * @param item
   */
  function applyItem(item) {
	    	
	    	//not applied before, start apply
	  		if (item !== undefined && item !== null) {
	    	var url = './apply';	    
		    var postMethod = 'POST';
		    var postObject = {
			        "username": username,
			        "apply": [{
			            "api_job_id" : item.api_job_id, 
			            "job_title" : item.job_title,
			            "location" : item.location,
			   		 	"company"  : item.company,
			            "job_snippet" : item.job_snippet,
			   		 	"job_url" : item.job_url
			         }]
			      }
		    var req = JSON.stringify(postObject);
		    ajax(postMethod, url, req,
		      // successful callback
		      function(res) {
		        var result = JSON.parse(res);
		        if (result.status === 'OK' || result.result === 'SUCCESS') {
		        	 $('#job-apply').text('Applied');
		        	 $('#job-apply').removeClass('modal-content-apply').addClass('modal-content-applied');
		        	 $("#job-apply").attr("disabled", true); 
		        	
		        }
		      });
	  		}
	       }
  
  // -------------------------------------
  // Create item list
  // -------------------------------------

  /**
   * List recommendation items base on the data received
   *
   * @param items - An array of item JSON objects
   */
  function listItems(items) {
    var itemList = document.querySelector('#item-list-detail');
    itemList.innerHTML = ''; // clear current results

    for (var i = 0; i < items.length; i++) {
      addItem(itemList, items[i]);
    }
    $('.row').mouseenter(function() {
  	  $(this).css({'background-color': '#1dcaff'});
  	  $(this).children().css({'background-color': '#1dcaff'});
      }).mouseleave(function() {
  	  $(this).css({'background-color': 'white'});
  	  $(this).children().css({'background-color': 'white'});	  
      });	
  }

 
  function hideJobDetails(){
	    $("#job-detail-modal").css("display", "none");
	    $("#job-detail-content").html("");
	  }
  
  function setAppliedItem(item) {
	  var url = './apply';
	    var params = 'username=' + username + '&' +  'jobid=' + item.api_job_id;
	    var req = JSON.stringify({});
	    // make AJAX call
	    ajax('GET', url + '?' + params, req, function(res) {
	      var items = JSON.parse(res);
	      if (!items || items.length === 0) {
	    	  setToApplyItemStyle(item);
	      } else {
	    	  setAppliedItemStyle(item);
	      }
	    }, function() {
	    	 setToApplyItemStyle(item);
	    });
	    setToApplyItemStyle(item);	 
  }
  
  function setLikedItem(item) {
	  var url = './like';
	    var params = 'username=' + username + '&' + 'jobid=' + item.api_job_id;
	    var req = JSON.stringify({});
	    // make AJAX call
	    ajax('GET', url + '?' + params, req, function(res) {
	      var items = JSON.parse(res);
	      if (!items || items.length === 0) {
	    	  setToLikeItemStyle(item);
	      } else {
	    	  setLikedItemStyle(item);
	      }
	    }, function() {
	    	setToLikeItemStyle(item);
	    });	 
	    setToLikeItemStyle(item);	     	    
  }
  
  function setToApplyItemStyle(item) {
	  console.log("the item is not applied");
	   $('#job-apply').attr("disabled", false);	
	   $("#job-apply").text("Apply");
	   if($('#job-apply').hasClass('modal-content-applied')) {
		  $('#job-apply').removeClass('modal-content-applied');		
	   }
	    $('#job-apply').addClass('modal-content-apply');	    
	    $('#job-apply').click(function() {applyItem(item);});
}
  
  function setAppliedItemStyle(item) {
	  console.log("the item is applied");
	  $("#job-apply").text("Applied");
 	 if($('#job-apply').hasClass('modal-content-apply')) {
	 		  $('#job-apply').removeClass('modal-content-apply');	 	   }
 	 $('#job-apply').addClass('modal-content-applied');	
 	 $("#job-apply").attr("disabled", true);
 	 
  }
  
  function setToLikeItemStyle(item) {
	    console.log("the item is not liked");
	    $("#job-like").text("Like");
	    if($('#job-like').hasClass('modal-content-liked')) {
	 		  $('#job-like').removeClass('modal-content-liked');
	 	   }
	    $('#job-like').addClass('modal-content-like');
	    $('#job-like').click(function() {favorItem(item)});
	    disFavorItem(item);
	
  }
  
  function setLikedItemStyle(item) {
	  console.log("the item is liked");
	  $("#job-like").text("Unlike");
 	 if($('#job-like').hasClass('modal-content-like')) {
	 		  $('#job-like').removeClass('modal-content-like');
	 	   }
	    	$('#job-like').addClass('modal-content-liked');	    	
	    	$('#job-like').click(function() {disFavorItem(item)});
	    	favorItem(item);
	       
  }
  /**
   * Add a single item to the list
   *
   * @param itemList - The <ul id="item-list"> tag (DOM container)
   * @param item - The item data (JSON object)
   *
   */
  function addItem(itemList, item) {
    var item_id = item.api_job_id;

    var row = $create('div', {
      id: 'item-' + item_id,
      className: 'row'
    });
   
 
    row.onclick = function() {  
    	
	    var indeed_url = item.job_url;
	   var cors_proxy = 'https://corsproxyserver.herokuapp.com/';
	    
	    var jobHead = $('#job-detail-header');	   
	    var jobContent = $("#job-detail-content");
	    jobContent.html("");
	   
	    $.get(cors_proxy + indeed_url.replace("http://", "https://"), function( data ) {
	        var html = $.parseHTML($(data).find("#jobDescriptionText").html());
	        jobContent.append(html);
	        $("#job-detail-modal").css("display", "block");
	        $('#job-detail-title').text(item.job_title);
			$('#job-detail-company').text(item.company);
			$('#job-detail-location').text(item.location);
	    });
	    //start getting the element first, if cannot get any item, enable apply and like button.
	    // else, disable apply button, disable like button.
	    setAppliedItem(item);
	    setLikedItem(item);	     		 
    };
    
    // set the data attribute ex. <li data-item_id="G5vYZ4kxGQVCR" data-favorite="true">
    row.dataset.item_id = item_id;
    
    // job title
    var section_title = $create('div');
    
    var title = $create('p', {className: 'jobtitle'})
    title.innerHTML = item.job_title;
   
    section_title.appendChild(title);
    row.appendChild(section_title);
    // company_name
    var section_company = $create('div')
   
    var company = $create('p', {
      className: 'jobcompany'
    });
    company.innerHTML = item.company; 
    section_company.appendChild(company);
    row.appendChild(section_company);
    // location
    var section_location = $create('div')
    var location = $create('p', {
      className: 'joblocation'
    });
    location.innerHTML = item.location;
    section_location.appendChild(location);
    row.appendChild(section_location);
    // snippet
    var section_snippet = $create('div')
    var snippet = $create('p', {
      className: 'jobsnippet'
    });
    snippet.innerHTML = item.job_snippet;
    section_snippet.appendChild(snippet);
    row.appendChild(section_snippet);  
    itemList.appendChild(row);
  }
  

  init();

})();