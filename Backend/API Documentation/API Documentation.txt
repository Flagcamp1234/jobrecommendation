Please follow the format below for API Documentation

http://opensource.indeedeng.io/api-documentation/docs/job-search/
Method Name: 

Request Parameters:

Parameter_Name  Data_Type  Required Description  Default_value:  

API Sample Request:

API Sample Response: {JSON Format}


1) Like and unlike Jobs
Method Name: ./like
Method: Get
// request parameters
    url = ./like
    params = 'username=' + username
    req = {}

Method: Post
//Request Parameters:
    url = ./like
    req = 
    {
      username: username,
      like: [{
         api_job_id : api_job_id, 
         job_title : job_title,
         location : location,
		 company  : company,
         job_snippet : job_snippet,
		 job_url : job_url
      }]
     }
========================
sample request:
======================== 
	 {
	"username":"1111",
	"like":[
	 {
	 	"api_job_id":"32fb9cda477feb5d",
	 	"job_title":"Java Developer",
	 	"location":"Austin, TX",
	 	"company":"State of Texas, RRC",
	 	"job_snippet":"The Worker will apply their expert knowledge of <b>Java</b> in the RRC agency technology stack to support the needs of internal ITS development project teams....",
	 	"job_url":"https://www.indeed.com/viewjob?jk=32fb9cda477feb5d&qd=DwH5aUFcigGJUdalCG1k4QMPJIcFSlm87e21Z1cw67qlrkKSykUpiCm5MCuCNgJZRBpGkt3CCrgVBWZGrZeLzFgRQ7KfHWcql6DScWml_yw&atk=1da8eq23ihc9g800&utm_source=publisher&utm_medium=organic_listings&utm_campaign=affiliate"
	 }	
	]
}
    
Method: Delete 
// request parameters
    url = ./like
    req = {
      username: username,
      like: ["api_job_id":api_job_id]
    }

========================
sample request:
========================
{
	"username":"1111",
	"like":[
	 {
	 	"api_job_id":"32fb9cda477feb5d"	 	
	 }	
	]
}


API Sample Response: {JSON Format}
{"result" : "SUCCESS"}



2) Apply Jobs
Method Name: ./apply
Method: Get
// request parameters
    url = ./apply
    params = 'username=' + username
    req = {}

Method: Post
//Request Parameters:
    url = ./apply
    req = 
    {
      username: username,
      apply: [{
         api_job_id : api_job_id, 
         job_title : job_title,
         location : location,
		 company  : company,
         job_snippet : job_snippet,
		 job_url : job_url
      }]
     }
========================
sample request:
========================

{
	"username":"1111",
	"apply":[
	 {
	 	"api_job_id":"32fb9cda477feb5d",
	 	"job_title":"Java Developer",
	 	"location":"Austin, TX",
	 	"company":"State of Texas, RRC",
	 	"job_snippet":"The Worker will apply their expert knowledge of <b>Java</b> in the RRC agency technology stack to support the needs of internal ITS development project teams....",
	 	"job_url":"https://www.indeed.com/viewjob?jk=32fb9cda477feb5d&qd=DwH5aUFcigGJUdalCG1k4QMPJIcFSlm87e21Z1cw67qlrkKSykUpiCm5MCuCNgJZRBpGkt3CCrgVBWZGrZeLzFgRQ7KfHWcql6DScWml_yw&atk=1da8eq23ihc9g800&utm_source=publisher&utm_medium=organic_listings&utm_campaign=affiliate"
	 }	
	]
}
=================
Sample Response
=================
{"result" : "SUCCESS"}

Parameter_Name  Data_Type  Required Description  Default_value:  

API Sample Request:

API Sample Response: {JSON Format}
{"result" : "SUCCESS"}

3) Search Jobs via user input
Method Name: ./search
Method: Get
// request parameters
    url = ./search
    params = 'search_term=' + search_term & 'location=' + location & 'username=' + username
    req = {}
	
//response: list of json object, sample response is below: 
[
{
         api_job_id : api_job_id, 
         job_title : job_title,
         location : location,
		 company  : company,
         job_snippet : job_snippet,
		 job_url : job_url
 }
]
 
===========
Sample Response
===========
[{"job_snippet":"Programming experience in 
<b>Java<\/b>, Spring, Hibernate, Web Services (RESTful, SOAP), JavaScript. When the situation calls for it, you\u2019ll be part of a team that...","api_job_id":"b54c8d06449a5172","company":"Google","location":"Austin, TX 78731","job_title":"Application Engineer","job_url":"http://www.indeed.com/viewjob?jk=b54c8d06449a5172&qd=DwH5aUFcigGJUdalCG1k4QMPJIcFSlm87e21Z1cw67qQM8Fp4swq5uKehVNPma9rTpxA_LZts4K3mkt3poQiWpPfDLpee6FYH-bY6rapYf0&indpubnum=125768650773419&atk=1da5bu4bg4trh800"}] 
   
Parameter_Name  Data_Type  Required Description  Default_value:  

API Sample Request:

API Sample Response: {JSON Format}
{"result" : "SUCCESS


4) Recommend jobs based on the requirement

Method Name: ./recommend
Method: Get
//request parameters
url = ./recommend
    params = username=1111&recWeight=0.6&userLocation=false
	
Response
//sample response: list of json object, sample response is below: 
[
{
         api_job_id : api_job_id, 
         job_title : job_title,
         location : location,
		 company  : company,
         job_snippet : job_snippet,
		 job_url : job_url
 }

 
5） Register

Method Name: ./register
Method: POST
//sample request
{
"username": "33333",
"password": "test",
"email": "xyz@gmail.com"
}

//sample response
{
"status": "OK"
}

6) Login

Method Name: ./login
Method: POST
//Sample request
{
"username": "33333",
"password": "test",
}

//sample response
{
    "status": "OK",
    "username": "33333"
}

7) Logout

Method Name: ./logout
Mehtod: GET
//Sample Response 
<html>
    <head>
        <title>Testing Page</title>
    </head>
    <body>
        <h1>This is a Sample Page</h1>
    </body>
</html

