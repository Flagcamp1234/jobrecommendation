(function() {

	var job_url = 'https://www.indeed.com/viewjob?jk=b54c8d06449a5172&qd=DwH5aUFcigGJUdalCG1k4QMPJIcFSlm87e21Z1cw67qQM8Fp4swq5uKehVNPma9rpoUkULQqlMEt6xdReeh8ytM9fF0VXd-hdKBbEDAtvMg&atk=1d9vlnnvpf25d800&utm_source=publisher&utm_medium=organic_listings&utm_campaign=affiliate';

	function init() {
		document.querySelector('#applied-btn').addEventListener('click', loadAppliedJobs);
		document.querySelector('#fav-btn').addEventListener('click', loadFavoriteJobs);
		validateSession();
	}

	function activeBtn(btnId) {
	    var btns = document.querySelectorAll('.list-group-item-action');

	    // deactivate all navigation buttons
	    for (var i = 0; i < btns.length; i++) {
	      btns[i].className = btns[i].className.replace(/\bactive\b/, '');
	    }

	    // active the one that has id = btnId
	    var btn = document.querySelector('#' + btnId);
	//    btn.className += ' active';
	    btn.className += 'active';
	  }

	function loadJobs() {
		console.log('loadJobs');
		activeBtn('nearby-btn');

	    // The request parameters
	    var url = './search';
	    var params = 'user_id=' + user_id + '&job_url' + job_url;
	    var data = null;

	    // display loading message
	    showLoadingMessage('Loading jobs...');

	    // make AJAX call
	    ajax('GET', url + '?' + params, data,
	      // successful callback
	      function(res) {
	      	var items = JSON.parse(res);
	      	if (!items || items.length === 0) {
	      		showWarningMessage('No job found.');
	      	} else {
	      		listItems(items);
	      	}
	      },
	      // failed callback
	      function() {
	      	showErrorMessage('Cannot load jobs.');
	      }
	      );
	}

	function loadFavoriteItems() {
		activeBtn('applied-btn');

	    // request parameters
	    var url = './apply';
	    var params = 'user_id=' + user_id;
	    var req = JSON.stringify({});

	    // display loading message
	    showLoadingMessage('Loading applied jobs...');

	    // make AJAX call
	    ajax('GET', url + '?' + params, req, function(res) {
	    	var items = JSON.parse(res);
	    	if (!items || items.length === 0) {
	    		showWarningMessage('No applied job.');
	    	} else {
	    		listItems(items);
	    	}
	    }, function() {
	    	showErrorMessage('Cannot load applied jobs.');
	    });
	}

	function loadFavoriteJobs() {
		activeBtn('fav-btn');

	    // request parameters
	    var url = './favorite';
	    var params = 'user_id=' + user_id;
	    var req = JSON.stringify({});

	    // display loading message
	    showLoadingMessage('Loading favorite jobs...');

	    // make AJAX call
	    ajax('GET', url + '?' + params, req, function(res) {
	    	var items = JSON.parse(res);
	    	if (!items || items.length === 0) {
	    		showWarningMessage('No favorite item.');
	    	} else {
	    		listItems(items);
	    	}
	    }, function() {
	    	showErrorMessage('Cannot load favorite jobs.');
	    });
	}



function ajax(method, url, data, successCallback, errorCallback) {
	var xhr = new XMLHttpRequest();

    xhr.open(method, url, true); // true: won't block other requests.

    xhr.onload = function() { // on readystatus change & response back.
      if (xhr.status === 200) { // status 200
      	successCallback(xhr.responseText);
      } else {
      	errorCallback();
      }
  };

    xhr.onerror = function() { // error while sending request. e.g. connection failed
    	console.error("The request couldn't be completed.");
    	errorCallback();
    };

    if (data === null) {
    	xhr.send();
    } else {
    	xhr.setRequestHeader("Content-Type",
    		"application/json;charset=utf-8");
    	xhr.send(data);
    }
}

function showLoadingMessage(msg) {
    var itemList = document.querySelector('#item-list');
    itemList.innerHTML = '<p class="notice"><i class="fa fa-spinner fa-spin"></i> ' +
      msg + '</p>';
  }

  function showWarningMessage(msg) {
    var itemList = document.querySelector('#item-list');
    itemList.innerHTML = '<p class="notice"><i class="fa fa-exclamation-triangle"></i> ' +
      msg + '</p>';
  }

  function showErrorMessage(msg) {
    var itemList = document.querySelector('#item-list');
    itemList.innerHTML = '<p class="notice"><i class="fa fa-exclamation-circle"></i> ' +
      msg + '</p>';
  }

init();
})();